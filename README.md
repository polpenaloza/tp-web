# Links
  - [reactjs](https://reactjs.org/)
  - [material-ui](https://material-ui.com/)
  - [redux](https://redux.js.org/)
  - [babeljs](https://babeljs.io/)
  - [webpack](https://webpack.js.org/)
  - [momentjs](https://momentjs.com)
  - [lodash](https://lodash.com/docs/4.17.10)
  - [chancejs](https://chancejs.com)

# Pre Requisitos
## Node
  - [nodejs](https://nodejs.org/en/download/)
## GIT
  - [git-scm](https://git-scm.com/downloads)
## Yarn
  - [yarnpkg](https://yarnpkg.com)

# Ejecutar el proyecto

Desde el terminal, ir hasta la ubicacion del proyecto y correr los siguientes comandos:

  - `yarn`
  - `yarn start`
