import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

window._ = require('lodash');
window.TextEncoder = require('text-encoding').TextEncoder;
window.moment = require('moment-timezone');

configure({ adapter: new Adapter() });
