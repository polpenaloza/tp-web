// @flow
import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

import { data, columns, setSelected } from '.';

@connect(
  ({ GridReducer: { selected } }) => ({ selected }),
  dispatch => ({
    dispatchSelected: val => dispatch(setSelected(val))
  })
)
class Grid extends Component {
  static get propTypes() {
    return {
      selected: PropTypes.array.isRequired,
      dispatchSelected: PropTypes.func.isRequired
    };
  }

  options = {
    filterType: 'dropdown',
    responsive: 'scroll',
    print: false,
    rowsSelected: this.props.selected,
    onRowsSelect: rows => {
      const { selected } = this.props;
      selected.filter(row => _.includes(rows, row.index));
      this.props.dispatchSelected({ selected: selected.concat(rows) });
    }
  };

  render = () => <MUIDataTable title="People List" data={data} columns={columns} options={this.options} />;
}

export default Grid;
