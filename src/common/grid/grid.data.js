import _ from 'lodash';
import moment from 'moment';
import Chance from 'chance';

const chance = new Chance();

export const data = _.times(chance.integer({ min: 5000, max: 9999 }), () => {
  const gender = chance.gender();
  const birthdayDate = chance.birthday({ type: chance.floating({ min: 0, max: 1 }) <= 0.3 ? 'Child' : '' });
  const id = chance.guid();
  const name = chance.first({ gender });
  const last = chance.last();
  const age = moment().diff(moment(birthdayDate), 'year');
  const birthdayString = moment(birthdayDate).format('DD/MM/YYYY');
  return [id, name, last, age, birthdayString, gender];
});

export const columns = [
  {
    name: 'Id',
    options: {
      filter: false,
      sort: true
    }
  },
  {
    name: 'Name',
    options: {
      filter: true,
      sort: true
    }
  },
  {
    name: 'Last Name',
    options: {
      filter: true,
      sort: true
    }
  },
  {
    name: 'Age',
    options: {
      filter: true,
      sort: true
    }
  },
  {
    name: 'Birthday',
    options: {
      filter: false,
      sort: true
    }
  },
  {
    name: 'Gender',
    options: {
      filter: true,
      sort: true
    }
  }
];
