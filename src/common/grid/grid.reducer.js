import { SET_SELECTED } from '.';

const initialState = {
  selected: []
};

export default (state = initialState, actions) => {
  const { selected, type } = actions;
  switch (type) {
    case SET_SELECTED:
      return Object.assign({}, state, { selected });
    default:
      return state;
  }
};
