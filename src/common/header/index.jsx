import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import Drawer from '../sidebar';

const styles = {
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -18,
    marginRight: 10
  }
};

class DenseAppBar extends Component {
  state = {
    toggle: false
  };

  render = () => {
    const { classes } = this.props;
    const { toggle } = this.state;
    return (
      <div className={classes.root}>
        <Drawer toggle={toggle} />
        <AppBar position="static">
          <Toolbar variant="dense">
            <IconButton aria-label="Menu" className={classes.menuButton} color="inherit" onClick={() => this.setState({ toggle: !toggle })}>
              <MenuIcon />
            </IconButton>
            <Typography color="inherit" variant="title">
              SAP - TFI
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  };
}

DenseAppBar.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(DenseAppBar);
