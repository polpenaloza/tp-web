import React, { Component } from 'react';
import { Card, CardHeader, CardContent, Divider, Grid } from '@material-ui/core';

import { GridComponent } from 'common';

export default class Home extends Component {
  render() {
    return (
      <Grid>
        <Card>
          <CardHeader align="center" title="Grid" />
          <Divider />
          <CardContent>
            <GridComponent />
          </CardContent>
        </Card>
      </Grid>
    );
  }
}
