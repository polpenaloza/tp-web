import React, { Fragment } from 'react';
import { hot } from 'react-hot-loader';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import CssBaseline from '@material-ui/core/CssBaseline';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

import Provider from './provider';
import routes from './routes';

const muiTheme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      light: '#2FA3E9',
      main: '#1689CF',
      dark: '#116AA1'
    }
  }
});

const App = () => (
  <BrowserRouter>
    <Provider>
      <Fragment>
        <CssBaseline />
        <MuiThemeProvider theme={muiTheme}>{routes}</MuiThemeProvider>
      </Fragment>
    </Provider>
  </BrowserRouter>
);

export default hot(module)(App);
