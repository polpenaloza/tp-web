import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import withRouter from 'react-router-dom/withRouter';

import configureStore from './redux.config';

@withRouter
class ProviderWithRouter extends Component {
  // eslint-disable-next-line react/destructuring-assignment
  store = configureStore(this.props.history);

  static propTypes = {
    children: PropTypes.node.isRequired,
    history: ReactRouterPropTypes.history.isRequired
  };

  render() {
    const { children } = this.props;
    return <Provider store={this.store}>{children}</Provider>;
  }
}

export default ProviderWithRouter;
