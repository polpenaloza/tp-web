import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import { GridReducer } from '../common/grid';

const rootReducer = combineReducers({
  GridReducer
});
// eslint-disable-next-line
const sagas = function* () {};

const isProd = process.env.NODE_ENV === 'production';

export default () => {
  const composer = isProd ? compose : composeWithDevTools;
  const store = createStore(rootReducer, composer(applyMiddleware(thunk)));

  if (module.hot) {
    module.hot.accept(rootReducer, () => {
      store.replaceReducer(rootReducer);
    });
  }

  return store;
};
