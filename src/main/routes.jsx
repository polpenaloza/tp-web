import Grid from '@material-ui/core/Grid';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Header } from 'common';
import Home from '../home';
import './routes.scss';

export default (
  <Grid container alignItems="flex-start" justify="center" className="route">
    <Grid item align="center" className="logo" xs={12}>
      <Header />
    </Grid>
    <Grid item className="content" xs={12}>
      <Grid container>
        <Grid item xs={12}>
          <Switch>
            <Route component={Home} path="/" />
          </Switch>
        </Grid>
      </Grid>
    </Grid>
  </Grid>
);
